import { render } from '@testing-library/react';

import LogoCloud from './logo-cloud';

describe('LogoCloud', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LogoCloud />);
    expect(baseElement).toBeTruthy();
  });
});
