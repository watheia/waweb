import clsx from 'clsx';
import { HtmlHTMLAttributes, SVGProps } from 'react';
import styles from './footer.module.css';

/* eslint-disable-next-line */
export interface FooterProps extends HtmlHTMLAttributes<HTMLDivElement> {}

const Logo = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={50}
    height={50}
    viewBox="0 0 512 512"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M196.43 479.173q-33.67 0-63.887-14.677-30.217-14.676-52.664-42.303-22.447-28.49-35.397-69.067t-12.95-91.514q0-48.347 12.95-89.788 12.95-42.303 36.26-72.52 24.174-31.08 56.98-48.347 33.67-18.13 74.248-18.13 49.21 0 79.427 19.857 31.08 18.993 43.167 54.39.863-8.633 2.59-18.993 2.59-10.36 4.317-18.994 2.59-9.497 5.18-16.403 2.59-6.907 4.316-9.497 1.727-1.727 5.18-1.727h92.378q3.453 0 5.18.864 1.726.863 0 6.043-5.18 17.267-10.36 45.757-5.18 27.627-9.497 61.297t-6.907 69.93q-2.59 36.26-2.59 70.794v20.72q0 37.124 10.36 47.484 11.224 10.36 26.764 10.36h6.906q2.59-.863 6.907-1.727 1.727 0 3.453.864 1.727.863 1.727 5.18v83.744q0 6.043-5.18 8.633-12.087 4.317-27.627 6.043-14.677 1.727-29.353 1.727-39.714 0-63.024-19.857-22.447-20.72-25.9-52.663l-.864-3.454q-19.856 35.397-50.937 56.117-31.08 19.857-81.153 19.857zm-42.304-223.605q0 29.354 6.043 50.937 6.907 20.72 17.267 34.534 11.224 12.95 25.037 19.857 13.813 6.043 28.49 6.043 21.584 0 37.124-9.497 15.54-10.36 25.037-26.763 10.36-17.267 14.676-38.85 4.317-22.447 4.317-45.757 0-53.527-20.72-82.018-19.857-28.49-56.117-28.49-38.85 0-60.434 32.807-20.72 32.807-20.72 87.197z"
      aria-label="\u03B1"
      style={{
        // fontWeight: 800,
        // fontSize: '863.339px',
        // fontFamily: "'Adobe Clean'",
        // InkscapeFontSpecification: "'Adobe Clean, Ultra-Bold'",
        fill: 'currentColor',
        // fillOpacity: 0.937255,
        strokeWidth: 2.61092,
        strokeLinecap: 'round',
        strokeLinejoin: 'bevel',
      }}
    />
  </svg>
);

export function Footer({ className, ...props }: FooterProps) {
  return (
    <footer
      className={clsx('footer', styles['container'], className)}
      {...props}
    >
      <div>
        <Logo />
        <p>
          Watheia Labs, LLC.
          <br />
          We make technology work for you.
        </p>
      </div>
      <div>
        <span className="footer-title">Social</span>
        <div className="grid grid-flow-col gap-4">
          <a>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              className="fill-current"
            >
              <path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z"></path>
            </svg>
          </a>
          <a>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              className="fill-current"
            >
              <path d="M19.615 3.184c-3.604-.246-11.631-.245-15.23 0-3.897.266-4.356 2.62-4.385 8.816.029 6.185.484 8.549 4.385 8.816 3.6.245 11.626.246 15.23 0 3.897-.266 4.356-2.62 4.385-8.816-.029-6.185-.484-8.549-4.385-8.816zm-10.615 12.816v-8l8 3.993-8 4.007z"></path>
            </svg>
          </a>
          <a>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              className="fill-current"
            >
              <path d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"></path>
            </svg>
          </a>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
