import clsx from 'clsx';
import { HtmlHTMLAttributes } from 'react';
import { Footer } from '../../molecules/footer';
import { NavBar } from '../../organisms/nav-bar';
import styles from './page-layout.module.css';

/* eslint-disable-next-line */
export interface PageLayoutProps extends HtmlHTMLAttributes<HTMLDivElement> {}

export function PageLayout({ className, children, ...props }: PageLayoutProps) {
  return (
    <div className={clsx(styles['container'], className)} {...props}>
      <NavBar />
      {children}
      <Footer />
    </div>
  );
}

export default PageLayout;
