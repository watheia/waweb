import clsx from 'clsx';
import { HtmlHTMLAttributes } from 'react';

/* eslint-disable-next-line */
export interface ButtonProps extends HtmlHTMLAttributes<HTMLButtonElement> {
  variant?:
    | 'primary'
    | 'secondary'
    | 'accent'
    | 'ghost'
    | 'info'
    | 'success'
    | 'warning'
    | 'error';
}

export function Button({
  className,
  children,
  variant,
  ...props
}: ButtonProps) {
  return (
    <button
      className={clsx(
        'btn',
        {
          'btn-primary': variant === 'primary',
          'btn-secondary': variant === 'secondary',
          'btn-accent': variant === 'accent',
          'btn-ghost': variant === 'ghost',
          'btn-info': variant === 'info',
          'btn-success': variant === 'success',
          'btn-warning': variant === 'warning',
          'btn-error': variant === 'error',
        },
        className
      )}
      {...props}
    >
      {children}
    </button>
  );
}

export default Button;
