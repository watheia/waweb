import { ComponentType, SVGProps } from 'react';

export declare type IconProps = SVGProps<SVGSVGElement> & {
  title?: string | undefined;
  titleId?: string | undefined;
};

export declare type IconType = ComponentType<IconProps>;
