import { IconType } from './icon';

/**
 * A NavItem represents an item in the site navigation. An item may have a url or child items but not both.
 */
export declare type NavItem = {
  name: string;
  icon?: IconType;
  description?: string;
} & ({ href: string } | { children: NavItem });
