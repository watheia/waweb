import { HomeView } from '@waweb/components';
import styles from './index.module.css';

export default function Index() {
  return <HomeView className={styles['page']} />;
}
