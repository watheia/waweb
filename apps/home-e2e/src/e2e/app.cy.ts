import { getGreeting } from '../support/app.po';

describe('home', () => {
  beforeEach(() => cy.visit('/'));

  it('should display tagline', () => {
    // Custom command example, see `../support/commands.ts` file
    cy.login('my-email@something.com', 'myPassword');

    // Function helper example, see `../support/app.po.ts` file
    getGreeting().should('have.text', 'We make technology work for you');
  });
});
