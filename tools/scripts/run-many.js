// run-many.js
// -------------
// A simple wrapper to run an nx command on all affected projects.
// Fixes issues with built in nx:affected commands not picking up
// changes in certain cases resulting in build failures
//////////////////////////////////////////////////////////////////////////////

const execSync = require('child_process').execSync;

const NX = 'yarn nx';

const target = process.argv[2];
const jobIndex = Number(process.argv[3]);
const jobCount = Number(process.argv[4]);
const isMaster = process.argv[5] === 'main';
const baseSha = isMaster ? 'main~1' : 'main';

function restArgs() {
  return process.argv
    .slice(6)
    .map((a) => `"${a}"`)
    .join(' ');
}

function main() {
  const affected = execSync(
    `${NX} print-affected --base=${baseSha} --target=${target}`
  ).toString('utf-8');
  const array = JSON.parse(affected).tasks.map((t) => t.target.project);
  array.sort();
  const sliceSize = Math.floor(array.length / jobCount);
  const projects =
    jobIndex < jobCount
      ? array.slice(sliceSize * (jobIndex - 1), sliceSize * jobIndex)
      : array.slice(sliceSize * (jobIndex - 1));

  console.log(`${target}:`, projects);
  if (projects.length > 0) {
    execSync(
      `${NX} run-many --target=${target} --projects=${projects.join(
        ','
      )} --parallel ${restArgs()}`,
      {
        stdio: [0, 1, 2],
      }
    );
  }
}

main();
